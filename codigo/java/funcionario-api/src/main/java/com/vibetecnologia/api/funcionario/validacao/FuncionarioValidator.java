package com.vibetecnologia.api.funcionario.validacao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.vibetecnologia.api.funcionario.model.Funcionario;
import com.vibetecnologia.baseapi.core.excecao.ExcecaoNegocio;
import com.vibetecnologia.baseapi.core.spring.configuration.messages.Messages;
import com.vibetecnologia.baseapi.core.utils.DocumentoUtils;

public class FuncionarioValidator {

	public void validar(Funcionario funcionario) {
		List<String> mensagens = new ArrayList<String>();

		if (funcionario.getIdEmpresa() == null) {
			mensagens.add(Messages.get("mensagem.campo.obrigatorio", "id.empresa"));
		}

		if (funcionario.getIdDepartamento() == null) {
			mensagens.add(Messages.get("mensagem.campo.obrigatorio", "id.departamento"));
		}

		if (StringUtils.isBlank(funcionario.getCpf())) {
			mensagens.add(Messages.get("mensagem.campo.obrigatorio", "cpf"));
		} else if (!DocumentoUtils.cpfValido(funcionario.getCpf())) {
			mensagens.add(Messages.get("mensagem.campo.invalido", "cpf"));
		}

		if (StringUtils.isBlank(funcionario.getNome())) {
			mensagens.add(Messages.get("mensagem.campo.obrigatorio", "nome"));
		}

		if (funcionario.getIdade() == null) {
			mensagens.add(Messages.get("mensagem.campo.obrigatorio", "idade"));
		}

		if (funcionario.getFuncao() == null) {
			mensagens.add(Messages.get("mensagem.campo.obrigatorio", "funcao"));
		}

		if (!mensagens.isEmpty()) {
			throw new ExcecaoNegocio(Messages.get("mensagem.objeto.invalido"), mensagens.get(0));
		}
	}

}
