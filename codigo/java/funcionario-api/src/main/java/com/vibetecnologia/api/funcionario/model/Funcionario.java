package com.vibetecnologia.api.funcionario.model;

import lombok.AllArgsConstructor;
import lombok.Data;

//import org.springframework.data.annotation.Id;
//import org.springframework.data.mongodb.core.mapping.Document;

//@Document(collection = "funcionario")

@Data
@AllArgsConstructor
public class Funcionario {

	// @Id
	private Long id;

	private Long idEmpresa;

	private Long idDepartamento;

	private String cpf;

	private String nome;

	private Integer idade;

	private String funcao;

}
