package com.vibetecnologia.api.funcionario.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.vibetecnologia.api.funcionario.model.Funcionario;
import com.vibetecnologia.api.funcionario.validacao.FuncionarioValidator;
import com.vibetecnologia.baseapi.core.excecao.ExcecaoNegocio;
import com.vibetecnologia.baseapi.core.spring.configuration.messages.Messages;
import com.vibetecnologia.baseapi.core.utils.StreamUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Api(tags = "Funcionario", description = "Operações da API de Funcionários.")
@RestController
@RequestMapping(path = "funcionarios")
public class FuncionarioController {

	private static final Logger LOGGER = LoggerFactory.getLogger(FuncionarioController.class);

	private Long funcionarioSequence = 0L;

	private List<Funcionario> funcionarios;

	// -------------------------------------------------------------
	// Inicialização.
	// -------------------------------------------------------------

	public FuncionarioController() {
		preencherDados();
	}

	public void preencherDados() {
		funcionarios = new ArrayList<Funcionario>();
		funcionarios.add(new Funcionario(funcionarioSequence++, 1L, 1L, "64525523093", "Leandro Pantoja Pereira", 35, "Analista de Sistemas"));
		funcionarios.add(new Funcionario(funcionarioSequence++, 1L, 1L, "93157754220", "Lucas Alcantara de Andrade", 25, "Analista de Sistemas"));
	}

	// -------------------------------------------------------------
	// Ações.
	// -------------------------------------------------------------

	@ApiOperation(value = "Operação que retorna a lista de todos os funcionários.")
	@GetMapping
	public List<Funcionario> buscarTodos(Funcionario filtro) throws Exception {
		LOGGER.info("Funcionário - buscar todos. Filtro: {}", filtro);

		List<Funcionario> funcionarios = StreamUtils.filter(this.funcionarios.stream(), filtro).collect(Collectors.toList());
		if (funcionarios.isEmpty()) {
			throw new ExcecaoNegocio(HttpStatus.NOT_FOUND, Messages.get("mensagem.nenhum.registro.encontrado"),
					Messages.get("mensagem.nenhum.registro.encontrado.parametros.informados"), null);
		}

		return funcionarios;

	}

	@ApiOperation(value = "Operação que retorna um funcionário através do identificador.")
	@GetMapping(path = "/{id}")
	public Funcionario buscarPorId(@PathVariable("id") Long id) {
		LOGGER.info("Funcionário - buscar por id: id = {}", id);

		Funcionario funcionario = funcionarios.stream().filter(f -> id.equals(f.getId())).findAny().orElse(null);
		if (funcionario == null) {
			throw new ExcecaoNegocio(HttpStatus.NOT_FOUND, Messages.get("mensagem.nenhum.registro.encontrado"),
					Messages.get("mensagem.nenhum.registro.encontrado.parametros.informados"), null);
		}

		return funcionario;
	}

	@ApiOperation(value = "Operação que retorna uma lista de funcionários que fazem parte de um departamento.")
	@GetMapping(path = "/departamento/{departamentoId}")
	public List<Funcionario> buscarPorDepartamento(@PathVariable("departamentoId") Long departamentoId) {
		LOGGER.info("Funcionário - buscar por departamento: id={}", departamentoId);

		List<Funcionario> funcionarios = this.funcionarios.stream().filter(f -> f.getIdDepartamento().equals(departamentoId)).collect(Collectors.toList());
		if (funcionarios.isEmpty()) {
			throw new ExcecaoNegocio(HttpStatus.NOT_FOUND, Messages.get("mensagem.nenhum.registro.encontrado"),
					Messages.get("mensagem.nenhum.registro.encontrado.parametros.informados"), null);
		}

		return funcionarios;
	}

	@ApiOperation(value = "Operação que retorna um funcionário que faz parte de uma empresa.")
	@GetMapping(path = "/empresa/{empresaId}")
	public List<Funcionario> buscarPorEmpresa(@PathVariable("empresaId") Long empresaId) {
		LOGGER.info("Funcionário - buscar por organização: id = {}", empresaId);

		List<Funcionario> funcionarios = this.funcionarios.stream().filter(f -> f.getIdEmpresa().equals(empresaId)).collect(Collectors.toList());
		if (funcionarios.isEmpty()) {
			throw new ExcecaoNegocio(HttpStatus.NOT_FOUND, Messages.get("mensagem.nenhum.registro.encontrado"),
					Messages.get("mensagem.nenhum.registro.encontrado.parametros.informados"), null);
		}

		return funcionarios;
	}

	@ApiOperation(value = "Operação que permite a inclusão de um novo funcionário.")
	@PostMapping(path = "/")
	@ResponseStatus(HttpStatus.CREATED)
	public Funcionario adicionar(@RequestBody Funcionario funcionario) {
		LOGGER.info("Funcionário - adicionar: {}", funcionario);

		new FuncionarioValidator().validar(funcionario);
		validarExisteRegistroMesmoCpf(funcionario);

		funcionario.setId(funcionarioSequence++);
		funcionarios.add(funcionario);

		return funcionario;
	}

	private void validarExisteRegistroMesmoCpf(Funcionario funcionario) {
		boolean registroJaExiste = funcionarios.stream().anyMatch(f -> !f.getId().equals(funcionario.getId()) && f.getCpf().equals(funcionario.getCpf()));
		if (registroJaExiste) {
			throw new ExcecaoNegocio(HttpStatus.CONFLICT, Messages.get("mensagem.registro.existente"), null, null);
		}
	}

	@ApiOperation(value = "Operação que permite a atualização de um funcionário.")
	@PutMapping(path = "/{id}")
	public Funcionario atualizar(@PathVariable("id") Long id, @RequestBody Funcionario funcionario) {
		LOGGER.info("Funcionário - atualizar: id = {}", id);

		new FuncionarioValidator().validar(funcionario);
		if (!id.equals(funcionario.getId())) {
			throw new ExcecaoNegocio(HttpStatus.CONFLICT, Messages.get("mensagem.erro.ids.diferentes"), null, null);
		}
		validarExisteRegistroMesmoCpf(funcionario);

		Funcionario funcionarioBase = buscarPorId(id);
		int index = funcionarios.indexOf(funcionarioBase);

		funcionarios.set(index, funcionario);

		return funcionario;
	}

	@ApiOperation(value = "Operação que permite a deleção de um funcionário.")
	@DeleteMapping(path = "/{id}")
	public Funcionario deletar(@PathVariable("id") Long id) {
		LOGGER.info("Funcionário - deletar: id = {}", id);

		Funcionario funcionario = buscarPorId(id);
		funcionarios.remove(funcionario);

		return funcionario;
	}

}