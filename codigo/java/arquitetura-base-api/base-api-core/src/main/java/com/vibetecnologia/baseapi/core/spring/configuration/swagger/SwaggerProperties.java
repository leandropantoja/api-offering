package com.vibetecnologia.baseapi.core.spring.configuration.swagger;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import lombok.Data;

@Configuration
@Component
public class SwaggerProperties {

	@Bean(name = "SwaggerProperties")
	@ConfigurationProperties(prefix = "swagger")
	public SwaggerPropertiesFields getInstance() {
		return new SwaggerPropertiesFields();
	}

	@Data
	public class SwaggerPropertiesFields {

		private String title;

		private String description;

	}

}
