package com.vibetecnologia.baseapi.core.excecao;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public abstract class Excecao extends RuntimeException {

	private HttpStatus codigo;

	private String mensagem;

	private String descricao;

	private String origem;

}
