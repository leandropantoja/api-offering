package com.vibetecnologia.baseapi.core.spring.configuration.swagger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicate;
import com.vibetecnologia.baseapi.core.spring.configuration.app.AppProperties;

import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

	@Autowired
	AppProperties appProperties;

	@Autowired
	SwaggerProperties swaggerProperties;

	@Bean
	public Docket createDocket() {
		Predicate<RequestHandler> basePackage = RequestHandlerSelectors.any();
		if (appProperties.getInstance().getBasePackage() != null) {
			basePackage = RequestHandlerSelectors.basePackage(appProperties.getInstance().getBasePackage());
		}

		Docket docket = new Docket(DocumentationType.SWAGGER_2).select().apis(basePackage).paths(PathSelectors.any()).build();

		if (getApiInfo() != null) {
			docket.apiInfo(getApiInfo());
		}

		// .useDefaultResponseMessages(false).globalResponseMessage(RequestMethod.GET, responseMessageForGET())

		return docket;
	}

	// private List<ResponseMessage> responseMessageForGET() {
	// return new ArrayList<ResponseMessage>() {
	//
	// private static final long serialVersionUID = 1L;
	// {
	// add(new ResponseMessageBuilder().code(500).message("500 message").build());
	// add(new ResponseMessageBuilder().code(500).message("500 message").build());
	// }
	// };
	// }

	private ApiInfo getApiInfo() {
		String title = swaggerProperties.getInstance().getTitle() != null ? swaggerProperties.getInstance().getTitle() : appProperties.getInstance()
				.getProjectName();

		return new ApiInfoBuilder().title(title).description(swaggerProperties.getInstance().getDescription())
				.version(appProperties.getInstance().getProjectVersion()).contact(new Contact("Vibe Tecnologia", "https://vibetecnologia.com/", null)).build();
	}

}