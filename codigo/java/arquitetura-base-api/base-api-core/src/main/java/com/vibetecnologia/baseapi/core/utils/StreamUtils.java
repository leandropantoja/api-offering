package com.vibetecnologia.baseapi.core.utils;

import java.lang.reflect.Field;
import java.util.stream.Stream;

public class StreamUtils {

	public static <T> Stream<T> filter(Stream<T> stream, T filter) {
		for (Field field : filter.getClass().getDeclaredFields()) {
			Object filterFieldValue = ReflectionUtils.invokeMethodGet(filter, field);

			if (filterFieldValue != null) {
				stream = stream.filter(o -> ReflectionUtils.invokeMethodGet(o, field).equals(filterFieldValue));
			}
		}

		return stream;
	}

}
