package com.vibetecnologia.baseapi.core.spring.configuration.controller;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.vibetecnologia.baseapi.core.excecao.ExcecaoNegocio;
import com.vibetecnologia.baseapi.core.spring.configuration.messages.Messages;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class TratamentoExcecao extends ResponseEntityExceptionHandler {

	@ExceptionHandler(ExcecaoNegocio.class)
	protected ResponseEntity<Object> tratarExcecaoNegocio(ExcecaoNegocio e, WebRequest request) {
		e.printStackTrace();
		RetornoExcecao response = new RetornoExcecao(e.getCodigo().value(), e.getMensagem(), e.getDescricao(), e.getOrigem());
		return new ResponseEntity<Object>(response, e.getCodigo());
	}

	@ExceptionHandler(Throwable.class)
	protected ResponseEntity<Object> tratarThrowable(Throwable e, WebRequest request) {
		e.printStackTrace();
		RetornoExcecao response = new RetornoExcecao(HttpStatus.INTERNAL_SERVER_ERROR.value(), Messages.get("mensagem.erro.inesperado.operacao"),
				Messages.get("mensagem.erro.inesperado.operacao.descricao"), null);
		return new ResponseEntity<Object>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception e, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
		return tratarThrowable(e, request);
	}

	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		RetornoExcecao response = new RetornoExcecao(status.value(), Messages.get("mensagem.requisicao.invalida"),
				Messages.get("mensagem.requisicao.invalida.descricao"), null);
		return new ResponseEntity<Object>(response, status);
	}

	@Override
	protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		RetornoExcecao response = new RetornoExcecao(status.value(), Messages.get("mensagem.requisicao.invalida"),
				Messages.get("mensagem.requisicao.parametro.invalido.descricao"), null);
		return new ResponseEntity<Object>(response, status);
	}

}
