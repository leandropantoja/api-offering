package com.vibetecnologia.baseapi.core.utils;

import org.apache.commons.lang3.StringUtils;

public abstract class DocumentoUtils {

	private static final int[] PESO_CPF = { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };

	private static final int[] PESO_CNPJ = { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };

	private DocumentoUtils() {

	}

	private static int calcularDigito(String str, int[] peso) {
		int soma = 0;
		for (int indice = str.length() - 1, digito; indice >= 0; indice--) {
			digito = Integer.parseInt(str.substring(indice, indice + 1));
			soma += digito * peso[peso.length - str.length() + indice];
		}
		soma = 11 - soma % 11;
		return soma > 9 ? 0 : soma;
	}

	public static final boolean cpfValido(String cpf) {
		if ((cpf == null) || (cpf.length() != 11))
			return false;

		boolean resposta = true;
		String c = cpf.substring(0, 1);
		for (int i = 1; i < cpf.length(); i++) {
			resposta = (resposta && c.equals(cpf.substring(i, i + 1)));
			c = cpf.substring(i, i + 1);
		}
		if (resposta)
			return false;

		Integer digito1 = calcularDigito(cpf.substring(0, 9), PESO_CPF);
		Integer digito2 = calcularDigito(cpf.substring(0, 9) + digito1, PESO_CPF);
		return cpf.equals(cpf.substring(0, 9) + digito1.toString() + digito2.toString());
	}

	public static final String formatarCPF(String cpf) {
		if (StringUtils.isBlank(cpf)) {
			return null;
		}

		if (cpf.trim().length() == 11) {
			StringBuffer stringBuffer = new StringBuffer(cpf);
			stringBuffer.insert(3, ".");
			stringBuffer.insert(7, ".");
			stringBuffer.insert(11, "-");
			return stringBuffer.toString();
		}

		return cpf;
	}

	public static final boolean cnpjValido(String cnpj) {
		if ((cnpj == null) || (cnpj.length() != 14))
			return false;

		Integer digito1 = calcularDigito(cnpj.substring(0, 12), PESO_CNPJ);
		Integer digito2 = calcularDigito(cnpj.substring(0, 12) + digito1, PESO_CNPJ);
		return cnpj.equals(cnpj.substring(0, 12) + digito1.toString() + digito2.toString());
	}

	public static final String formatarCNPJ(String cnpj) {
		if (StringUtils.isBlank(cnpj)) {
			return null;
		}

		if (cnpj.trim().length() == 14) {
			StringBuffer stringBuffer = new StringBuffer(cnpj);
			stringBuffer.insert(2, ".");
			stringBuffer.insert(6, ".");
			stringBuffer.insert(10, "/");
			stringBuffer.insert(15, "-");
			return stringBuffer.toString();
		}

		return cnpj;
	}

	public static final String formatarCPFCNPJ(String cpfCnpj) {
		if (StringUtils.isBlank(cpfCnpj)) {
			return null;
		}

		if (cpfCnpj.trim().length() == 11) {
			return formatarCPF(cpfCnpj);
		}

		if (cpfCnpj.trim().length() == 14) {
			return formatarCNPJ(cpfCnpj);
		}

		return cpfCnpj;
	}

	public static final String formatarCEP(String cep) {
		if (StringUtils.isBlank(cep)) {
			return null;
		}

		if (cep.trim().length() == 8) {
			StringBuffer stringBuffer = new StringBuffer(cep);
			stringBuffer.insert(2, ".");
			stringBuffer.insert(6, "-");
			return stringBuffer.toString();
		}

		return cep;
	}

}
