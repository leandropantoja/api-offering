package com.vibetecnologia.baseapi.core.spring.configuration.app;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import lombok.Data;

@Configuration
@Component
public class AppProperties {

	@Bean(name = "AppProperties")
	@ConfigurationProperties(prefix = "app")
	public AppPropertiesFields getInstance() {
		return new AppPropertiesFields();
	}

	@Data
	public class AppPropertiesFields {

		private String basePackage;

		private String projectName;

		private String projectVersion;

	}

}
