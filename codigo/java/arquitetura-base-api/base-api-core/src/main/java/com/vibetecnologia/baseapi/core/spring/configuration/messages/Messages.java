package com.vibetecnologia.baseapi.core.spring.configuration.messages;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Component;

@Component
public class Messages implements Serializable {

	private static MessageSource messageSource;

	@Autowired
	public Messages(MessageSource messageSource) {
		Messages.messageSource = messageSource;
	}

	public static String get(String key, Object... args) {
		for (int i = 0; i < args.length; i++) {
			if (args[i] instanceof String && containsKey((String) args[i])) {
				args[i] = get((String) args[i]);
			}
		}

		return Messages.messageSource.getMessage(key, args, null);
	}

	private static boolean containsKey(String key) {
		try {
			return Messages.messageSource.getMessage(key, null, null) != null;
		} catch (NoSuchMessageException e) {
			return false;
		}
	}

}
