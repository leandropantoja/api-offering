package com.vibetecnologia.baseapi.core.utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import org.apache.commons.lang3.reflect.MethodUtils;

import com.vibetecnologia.baseapi.core.excecao.ExcecaoInfraEstrutura;

public class ReflectionUtils {

	public static Object invokeMethodGet(Object object, Field field) {
		try {
			return MethodUtils.invokeMethod(object,
					"get".concat(field.getName().replaceFirst(field.getName().substring(0, 1), field.getName().substring(0, 1).toUpperCase())));
		} catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
			throw new ExcecaoInfraEstrutura(e.getMessage());
		}
	}

	@SuppressWarnings("rawtypes")
	public static void invokeMethodSet(Object object, Field field, Object value) {
		try {
			if (value != null) {
				MethodUtils.invokeMethod(object,
						"set".concat(field.getName().replaceFirst(field.getName().substring(0, 1), field.getName().substring(0, 1).toUpperCase())), value);
			} else {
				Object[] values = { value };
				Class[] types = { field.getType() };

				MethodUtils.invokeMethod(object,
						"set".concat(field.getName().replaceFirst(field.getName().substring(0, 1), field.getName().substring(0, 1).toUpperCase())), values,
						types);
			}
		} catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
			throw new ExcecaoInfraEstrutura(e.getMessage());
		}
	}

}
