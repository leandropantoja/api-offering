package com.vibetecnologia.baseapi.core.excecao;

import org.springframework.http.HttpStatus;

public class ExcecaoNegocio extends Excecao {

	private static final HttpStatus STATUS_PADRAO = HttpStatus.BAD_REQUEST;

	public ExcecaoNegocio(String mensagem) {
		super(STATUS_PADRAO, mensagem, null, null);
	}

	public ExcecaoNegocio(String mensagem, String descricao) {
		super(STATUS_PADRAO, mensagem, descricao, null);
	}

	public ExcecaoNegocio(String mensagem, String descricao, String origem) {
		super(STATUS_PADRAO, mensagem, descricao, origem);
	}

	public ExcecaoNegocio(HttpStatus codigo, String mensagem, String descricao, String origem) {
		super(codigo, mensagem, descricao, origem);
	}

}
