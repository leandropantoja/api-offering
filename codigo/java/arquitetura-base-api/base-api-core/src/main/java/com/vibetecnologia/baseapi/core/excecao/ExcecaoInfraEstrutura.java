package com.vibetecnologia.baseapi.core.excecao;

import org.springframework.http.HttpStatus;

public class ExcecaoInfraEstrutura extends Excecao {

	private static final HttpStatus STATUS_PADRAO = HttpStatus.INTERNAL_SERVER_ERROR;

	public ExcecaoInfraEstrutura(String mensagem) {
		super(STATUS_PADRAO, mensagem, null, null);
	}

	public ExcecaoInfraEstrutura(String mensagem, String descricao) {
		super(STATUS_PADRAO, mensagem, descricao, null);
	}

	public ExcecaoInfraEstrutura(String mensagem, String descricao, String origem) {
		super(STATUS_PADRAO, mensagem, descricao, origem);
	}

	public ExcecaoInfraEstrutura(HttpStatus codigo, String mensagem, String descricao, String origem) {
		super(codigo, mensagem, descricao, origem);
	}

}
