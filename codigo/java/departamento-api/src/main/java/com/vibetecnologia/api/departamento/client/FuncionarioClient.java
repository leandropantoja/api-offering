package com.vibetecnologia.api.departamento.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.vibetecnologia.api.departamento.model.Funcionario;

@FeignClient(name = "funcionario-api")
public interface FuncionarioClient {

	@GetMapping("/departamento/{departamentoId}")
	List<Funcionario> buscarPorDepartamento(@PathVariable("departamentoId") Long departamentoId);
	
}
