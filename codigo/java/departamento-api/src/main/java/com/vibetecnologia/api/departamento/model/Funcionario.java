package com.vibetecnologia.api.departamento.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Funcionario {

	//@Id
	private Long id;
	private String nome;
	private int idade;
	private String funcao;

	public Funcionario() {

	}
	
	
	public Funcionario(Long id, String nome, int idade, String funcao) {
		super();
		this.id = id;
		this.nome = nome;
		this.idade = idade;
		this.funcao = funcao;
	}
	

	@Override
	public String toString() {
		return "Funcionario [id=" + id + ", nome=" + nome + ", funcao=" + funcao + "]";
	}



}
