package com.vibetecnologia.api.departamento.model;

import java.util.ArrayList;
import java.util.List;

//import org.springframework.data.annotation.Id;
//import org.springframework.data.annotation.Transient;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter

public class Departamento {

	//@Id
	private Long id;
	private Long empresaId;
	private String nome;
	//@Transient
	private List<Funcionario> funcionarios = new ArrayList<>();

	public Departamento() {
		
	}

	public Departamento(Long empresaId, String nome) {
		super();
		this.empresaId = empresaId;
		this.nome = nome;
	}
	
	public Departamento(Long id, Long empresaId, String nome) {
		super();
		this.id = id;
		this.empresaId = empresaId;
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Departmento [id=" + id + ", empresaId=" + empresaId + ", nome=" + nome + "]";
	}

}
