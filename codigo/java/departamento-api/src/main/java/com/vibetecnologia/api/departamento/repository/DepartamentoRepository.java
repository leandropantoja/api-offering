package com.vibetecnologia.api.departamento.repository;

import java.util.List;

import com.vibetecnologia.api.departamento.model.Departamento;


//public interface DepartamentoRepository extends CrudRepository<Departamento, String> {
public interface DepartamentoRepository {

	List<Departamento> findByOrganizationId(Long organizationId);
	
}
