package com.vibetecnologia.api.departamento.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vibetecnologia.api.departamento.client.FuncionarioClient;
import com.vibetecnologia.api.departamento.model.Departamento;
import com.vibetecnologia.api.departamento.model.Funcionario;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Api(tags = "Departamento", description = "Operações da API de Departamento.")
@RestController
@RequestMapping(path = "departamento")
public class DepartamentoController {

	public DepartamentoController() {
		departamentos = new ArrayList<Departamento>();
		this.preencherDados();
	} 

	public void preencherDados() {
		departamentos.add(new Departamento(1L, 1L,"Fábrica de Software"));
		departamentos.add(new Departamento(2L, 1L,"Suporte e Sustentação"));
		departamentos.add(new Departamento(3L, 1L,"Comercial"));
		departamentos.add(new Departamento(4L, 1L,"Marketing"));
		departamentos.add(new Departamento(4L, 1L,"RH"));
	}

	private List<Departamento> departamentos;

	private static final Logger LOGGER = LoggerFactory.getLogger(DepartamentoController.class);

	
	@Autowired
	FuncionarioClient funcionarioClient;

	@ApiOperation(value = "Operação que retorna um funcionário de um departamento")
	@GetMapping("/feign")
	public List<Funcionario> listRest() {
		return funcionarioClient.buscarPorDepartamento(1L);
	}
	
	@ApiOperation(value = "Operação que retorna a lista de todos os departamentos")
	@GetMapping("/")
	public List<Departamento> buscarTodos() {
		LOGGER.info("Departamento - buscar todos");
		return departamentos;
		//return repository.findAll();
	}
	
	@ApiOperation(value = "Operação que permite a inclusão de um novo departamento")
	@PostMapping("/")
	public Departamento adicionar(@RequestBody Departamento departamento) {
		LOGGER.info("Departamento - adicionar: {}", departamento);

		//TODO Testar se já existe registro usando buscarPorId
		departamentos.add(departamento);
		return departamento;

		//return repository.save(departamento);
	}

	@ApiOperation(value = "Operação que retorna um departamento através do identificador")
	@GetMapping("/{id}")
	public Departamento buscarPorId(@PathVariable("id") Long id) {
		LOGGER.info("Departamento - buscar por id: id={}", id);

		Departamento departamento = departamentos.stream()
				.filter(d -> id.equals(d.getId()))
				.findAny()
				.orElse(null);

		//TODO LANÇAR EXCEÇÃO NOT FOUND 404
		return departamento;
		//return repository.findById(id).get();
	}

	//TODO Resolver código duplicado
	@ApiOperation(value = "Operação que retorna departamentos que fazem parte de uma empresa")
	@GetMapping("/empresa/{empresaId}")
	public List<Departamento> buscarPorEmpresa(@PathVariable("empresaId") Long empresaId) {
		LOGGER.info("Departamento - buscar por empresa: id={}", empresaId);

		List<Departamento> departamentos =  new ArrayList<Departamento>();

		Iterator<Departamento> iterator = this.departamentos.iterator();
		while (iterator.hasNext()) {
			Departamento departamento = iterator.next();
			if (departamento.getEmpresaId().equals(empresaId)) {
				departamentos.add(departamento);
			}
		}	   
		//TODO Testar lista vazia

		return departamentos;

	}

	@ApiOperation(value = "Operação que retorna udepartamentos que fazem parte de uma empresa e que tenham funcionários")
	@GetMapping("/empresa/{empresaId}/com-funcionarios")
	public List<Departamento> buscarPorEmpresaComFuncionarios(@PathVariable("empresaId") Long empresaId) {
		LOGGER.info("Department buscar por e com funcionários: empresaId={}", empresaId);
		List<Departamento> departamentos = this.buscarPorEmpresa(empresaId);
		//TODO TESTAR LISTA VAZIA. SE NÃO HOUVER DEPARTAMENTOS, NÃO É NECESSÁRIO BUSCAR OS FUNCIONARIOS
		departamentos.forEach(d -> d.setFuncionarios(funcionarioClient.buscarPorDepartamento(d.getId())));

		return departamentos;
	}

}
